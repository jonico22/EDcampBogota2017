<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('explore', 'SeriesController@index')->name('explore');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/series/{series}', 'SeriesController@show')->name('series.show');
    Route::get('/chapters/{chapters}', 'ChaptersController@show')->name('chapters.show');

    Route::get('profile', 'UserController@profile')->name('users.profile');
    Route::put('profile/{user}', 'UserController@update')->name('users.update');

    Route::get('subscription', 'SubscriptionsController@subscribe')->name('subscriptions.subscribe');
    Route::post('subscription', 'SubscriptionsController@payu')->name('subscriptions.payu');

    Route::get('historical', 'HistoricalsController@index')->name('users.historical');
});

Route::group(['prefix' => 'admin', 'middleware' => 'isAdmin'], function () {
    # Users
    Route::get('users', 'UserController@index')->name('admin.users.index');
    Route::get('users/{users}/edit', 'UserController@edit')->name('admin.users.edit');

    # Series
    Route::match(['get', 'head'], '/series', 'SeriesController@index')->name('admin.series.index');
    Route::post('/series', 'SeriesController@store')->name('admin.series.store');
    Route::match(['get', 'head'], '/series/create', 'SeriesController@create')->name('admin.series.create');
    Route::match(['get', 'head'], '/series/{series}', 'SeriesController@show')->name('admin.series.show');
    Route::match(['put', 'patch'], '/series/{series}', 'SeriesController@update')->name('admin.series.update');
    Route::match(['get', 'head'], '/series/{series}/edit', 'SeriesController@edit')->name('admin.series.edit');

    # Chapters
    Route::post('/chapters/{serie}', 'ChaptersController@store')->name('admin.chapters.store');
    Route::match(['get', 'head'], '/chapters/{serie}/create', 'ChaptersController@create')->name('admin.chapters.create');
    Route::match(['get', 'head'], '/chapters/{chapters}', 'ChaptersController@show')->name('admin.chapters.show');
    Route::match(['put', 'patch'], '/chapters/{chapters}', 'ChaptersController@update')->name('admin.chapters.update');
    Route::match(['get', 'head'], '/chapters/{chapters}/edit', 'ChaptersController@edit')->name('admin.chapters.edit');

    # Prices
    Route::match(['get', 'head'], '/prices', 'PricesController@index')->name('admin.prices.index');
    Route::post('/prices', 'PricesController@store')->name('admin.prices.store');
    Route::match(['get', 'head'], '/prices/create', 'PricesController@create')->name('admin.prices.create');
    Route::match(['put', 'patch'], '/prices/{prices}', 'PricesController@update')->name('admin.prices.update');
    Route::match(['get', 'head'], '/prices/{prices}/edit', 'PricesController@edit')->name('admin.prices.edit');
});
